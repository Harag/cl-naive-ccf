(defsystem "cl-naive-ccf.tests"
  :description "Tests for cl-naive-ccf."
  :version "2022.9.2"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-ccf :cl-naive-tests
               :cl-fad)
  :components (
               (:file "tests/package")
               (:file "tests/tests" :depends-on ("tests/package"))))

