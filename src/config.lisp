(in-package :cl-naive-ccf)

;;TODO: Make thread safe.
(defparameter *configs* (make-hash-table :test #'equalp))

(defmacro define-config ((&optional name) &body body)
  "Stores a config by name for easy retrieval later.

BODY should be parsable by cl-naive-dom:with-dom."
  `(eval-when (:load-toplevel :execute)
     (setf (gethash (or (cl-naive-dom:tag-convert ',name) :default) *configs*)
           (cl-naive-dom:with-dom
             ,@body))))

(defun register-config (name config &key overwrite-p)
  "A way to register configs that where not created with DEFINE-CONFIG.
For example when a persisted config is loaded from file."
  (let ((registered (gethash (cl-naive-dom:tag-convert name) *configs*)))
    (if registered
        (if overwrite-p
            (setf (gethash (cl-naive-dom:tag-convert name) *configs*) config)
            (error "Config alreay registered."))
        (setf (gethash (cl-naive-dom:tag-convert name) *configs*) config))))

(defun persist-config (config file &key (keywords-p t))
  "Persist config to file.

If the DOM is written with keywords-p set to nil then you need to
define the tags that are used before trying to load the DOM from file
again, else the parser wont successfully parse the file contents."
  (cl-naive-dom:persist config file :keywords-p keywords-p))

(defun load-config (file)
  "Load config from file. This does not register the config as well it
is left up to the user.

If the DOM was written with keywords-p set to nil then you need to
define the tags that are used before trying to load the DOM from file
again, else the parser wont successfully parse the file contents."
  (cl-naive-dom:load-dom file))

(defun config (&optional name)
  "Fetches a config by name.

If NAME is not supplied it the :DEFAULT config will be fetched."
  (gethash (cl-naive-dom:tag-convert (or name :default)) *configs*))

(defun ensure-config (config)
  "Returns the CONFIG if it is an element or fetches a config from
storage by config as name."
  (if (typep config 'cl-naive-dom:element)
      config
      (config config)))

(defparameter *config* nil
  "Special variable used to specify config in context.")

(defun current-config ()
  "Current config in context."
  *config*)

(defmacro with-config ((config) &body body)
  "Convenience macro that sets the config to be used in context."
  `(let ((*config* (ensure-config ,config)))
     ,@body))

(defgeneric select-setting (selector &rest rest &key config &allow-other-keys)
  (:documentation "Generic function that can be specialized to retrieve settings. This
function is called SELECT because it will return a setting or many
settings.

Users are encouraged to create their own specialized implementations
to deal with complex configuration structures."))

(defun ensure-list (x)
  (if (listp x) x (list x)))

(defmethod select-setting (selector &rest rest &key config &allow-other-keys)
  "The default implementation of select-setting to select elements or
values from a configuration using TAG names and/or ATTIBUTE.

SELECTOR SYNTAX:

If the SELECTOR is a SYMBOL then all elements with a TAG name that is
eql to SELECTOR will be returned.

If the SELECTOR is a list and the car is AND then if the tag path
contains all TAGS after the AND the element will be selected.

If the SELECTOR is a list and the car is OR then if the current TAG is
a memeber the TAGS after the OR the element will be selected.

If the SELECTOR is a list of SYMBOLS then it represents a PATH or a
partial PATH and all elements that match the path or partial PATH are
returned.

If the SELECTOR is a function then the function will be called to
affect selection. The function signature should be (ELEMENT PATH) and
the function must return what should be selected. What should be
selected is up to the user, but typically it would be an ELEMENT or
ATTRIBUTE or ATTRIBUTE VALUE.

CONFIG is the config to search else (CURREN-CONFIG) will be used.

REST, if :ATTRIBUTE is supplied the value of the attribute of the
first setting found will be returned.

"
  ;; TODO: Create a way to specify allowed attributes seperate from the actual select
  ;; (check-parameters rest '(:attribute))

  (let ((*config* (or (and config (ensure-config config))
                      (or *config* (config :default)))))

    (let ((settings (cond ((and (listp selector)
                                (member (car selector) '(and or) :test #'equalp))
                           (cond ((equalp (car selector) 'or)
                                  (cl-naive-dom:extract (ensure-list (cdr selector))
                                                        (current-config)))
                                 ((equalp (car selector) 'and)
                                  (let ((selector-names (cl-naive-dom::convert-tags
                                                         (cdr selector))))
                                    (cl-naive-dom:query-selector
                                     (current-config)
                                     (lambda (element path)
                                       (let ((path-names (nreverse
                                                          (mapcar #'cl-naive-dom::tag path))))
                                         ;;set-difference ignores order and length differences
                                         (when (not (set-difference selector-names path-names))
                                           element))))))))
                          ((symbolp selector)
                           (cl-naive-dom:extract (list selector)
                                                 (current-config)))
                          ((listp selector)
                           (let ((selector-names (cl-naive-dom::convert-tags selector)))
                             (cl-naive-dom:query-selector
                              (current-config)
                              (lambda (element path)
                                (let ((path-names (nreverse (mapcar #'cl-naive-dom::tag path))))

                                  (when (or
                                         (equalp path-names selector-names)
                                         (and (intersection selector-names path-names)
                                              (>= (length path-names) (length selector-names))

                                              (equal selector-names
                                                     (reverse (subseq (reverse path-names) 0
                                                                      (length selector-names))))))
                                    element))))))
                          ((functionp selector)
                           (cl-naive-dom:query-selector
                            (current-config)
                            selector)))))
      (when settings
        (if (getf rest :attribute)
            (cl-naive-dom:attribute
             (car settings)
             (getf rest :attribute))
            settings)))))
