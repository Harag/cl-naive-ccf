(in-package :cl-naive-ccf)

(defparameter *tag-attributes* (make-hash-table :test #'equalp))

(defun set-allowed-attributes (tag attribute-list)
  "To sanely do inheritance tags that should inherit attributes must
define their allowed attributes.

If no allowed-attributes exist for a tag no inheritance is done by the
defualt rule.
"
  (setf (gethash (cl-naive-dom::tag-convert tag) *tag-attributes*) attribute-list))

(defun allowed-attributes (tag)
  "A list of the TAG's allowed attributes."
  (gethash (cl-naive-dom::tag-convert tag) *tag-attributes*))

(defun allowed-attribute-p (tag attribute)
  "Checks if ATTRIBUTE is a member of the allowed attributes for the TAG."
  (member attribute (allowed-attributes tag) :test #'equal))

;;The default rule set up for inheritance.
(eval-when (:compile-toplevel :load-toplevel :execute)
  (define-rule (default-inheritance () :ruleset :inheritance)
    (when (cl-naive-dom:path)
      (dolist (parent (cl-naive-dom:path))
        (dolist (attribute (cl-naive-dom:attributes parent))
          (when (allowed-attribute-p (cl-naive-dom:tag (cl-naive-dom:element))
                                     (cl-naive-dom:key attribute))
            (when (cl-naive-dom:value attribute)
              (cl-naive-dom:set-attribute (cl-naive-dom:element)
                                          (cl-naive-dom:key attribute)
                                          (cl-naive-dom:value attribute)))))))
    (cl-naive-dom:element)))

