(in-package :common-lisp-user)

(defpackage :cl-naive-ccf
  (:use :cl)
  (:export

   ;;config
   :define-config
   :register-config
   :persist-config
   :load-config
   :config
   :ensure-config
   :with-config
   :current-config
   :select-setting

   ;;rules
   :ruleset
   :name
   :rules
   :cascading-ruleset
   :inheritance-ruleset
   :ensure-ruleset
   :register-ruleset
   :ruleset
   :find-rule
   :remove-rule
   :add-rule
   :ensure-rule
   :rule-selector
   :define-rule
   :apply-rule
   :apply-rules

   ;;inheritance
   :set-allowed-attributes
   :allowed-attributes
   :allowed-attribute-p))
