(in-package :cl-naive-ccf)

(eval-when (:compile-toplevel :load-toplevel :execute)

  (defclass ruleset ()
    ((name :initarg :name
           :initform nil
           :accessor name
           :documentation "Name of the ruleset.")
     (rules :initarg :rules
            :initform nil
            :accessor rules
            :documentation "List of transformations that makes up a ruleset."))
    (:documentation "Using a class to wrap rules in to enable the effcient adding and
removing of rules."))

  (defparameter *rulesets* (make-hash-table :test #'equalp)
    "A hash table of RULESET(s). The default cascading ruleset is
registered as :CASCADING and the default inheritance ruleset is
registered as :INHERITANCE.")

  (defun cascading-ruleset ()
    "Returns the default cascading ruleset."
    (let ((ruleset (gethash :cascading *rulesets*)))
      (unless ruleset
        (setf ruleset (make-instance 'ruleset :name :cascading))
        (setf (gethash :cascading *rulesets*) ruleset))
      ruleset))

  (defun inheritance-ruleset ()
    "Returns the default inheritance ruleset."
    (let ((ruleset (gethash :inheritance *rulesets*)))
      (unless ruleset
        (setf ruleset (make-instance 'ruleset :name :inheritance))
        (setf (gethash :inheritance *rulesets*) ruleset))
      ruleset))

  (defun ensure-ruleset (ruleset &key (create-p nil))
    "Returns RULESET if it is of type RULESET or finds the ruleset by
name. If the ruleset does not exist a new ruleset is created and
registered.

If the ruleset does not exist you can force creation by setting CREATE-P to T."
    (if (typep ruleset 'ruleset)
        ruleset
        (cond ((equal ruleset :cascading)
               (cascading-ruleset))
              ((equal ruleset :inheritance)
               (inheritance-ruleset))
              (t
               (let ((ruleset% (gethash ruleset *rulesets*)))
                 (unless ruleset%
                   (when create-p
                     (setf ruleset% (make-instance 'ruleset :name ruleset))
                     (setf (gethash ruleset *rulesets*) ruleset%)))
                 ruleset%)))))

  (defun register-ruleset (name rules &key (if-exists-replace-p nil))
    "Registers a ruleset.

NAME is the name of the rules set. :CASCADING and :INHERITANCE are
used for the default rules sets.

RULES is the set of rules to be registered it must be an empty list or
a list of transforms."

    (when (and (not if-exists-replace-p) (gethash name *rulesets*))
      (error "Replacing existing ruleset not explicitly specified."))

    (setf (gethash name *rulesets*) (make-instance 'ruleset :name name :rules rules)))

  (defun ruleset (name)
    "Returns a list of rules assosicated with the NAME."
    (gethash name *rulesets*))

  (defun find-rule (name &key ruleset)
    "Returns a rule.

RULESET is the ruleset to search. If no RULESET is specified the
CASCADING-RULESET and then the INHERITANCE-RULESET is search."
    (if ruleset
        (find name (rules (ensure-ruleset ruleset)) :key #'cl-naive-dom:transform-name)
        (or (find name (rules (cascading-ruleset)) :key #'cl-naive-dom:transform-name)
            (find name (rules (inheritance-ruleset)) :key #'cl-naive-dom:transform-name))))

  (defun remove-rule (rule &key (ruleset :cascading))
    "Removes a rule from a ruleset.

RULE is a transform instance or a symbol representing a rule
name.

RULESET is a ruleset or the name of a ruleset. Defaults to CASCADING-RULESET."

    (let ((ruleset% (ensure-ruleset ruleset)))
      (unless ruleset%
        (error "ruleset ~A does not exist." ruleset))

      (setf (rules ruleset%)
            (remove (ensure-rule rule :ruleset ruleset%) (rules ruleset%)))))

  (defun add-rule (rule &key (ruleset :cascading))
    "Adds a rule to a ruleset.

If a rule with the same name is found to exist it is removed and
replaced with the supplied rule.

RULESET is the ruleset to add the rule to. If no RULESET is
supplied the CASCADING-RULESET is used.

If the RULESET does not exits a new one is created to add the RULE
to.
"
    (let ((ruleset% (ensure-ruleset ruleset)))
      (unless ruleset%
        (error "ruleset ~A does not exist." ruleset))

      (let ((exists (find-rule (cl-naive-dom::transform-name rule)
                               :ruleset ruleset%)))
        (when exists
          (remove-rule exists :ruleset ruleset%))

        (pushnew rule (rules ruleset%))))
    rule)

  (defun ensure-rule (rule &key (ruleset :cascading))
    "Returns RULE if its a transform else it searches the RULESET by name."
    (let  ((ruleset% (ensure-ruleset ruleset)))
      (unless ruleset%
        (error "ruleset ~A does not exist." ruleset))

      (if (typep rule 'cl-naive-dom:transform)
          rule
          (find-rule rule :ruleset ruleset%))))

  (defgeneric rule-selector (selector &rest rest &key or &allow-other-keys)
    (:documentation "Used by define-rule to create the correct selector form."))

  (defmethod rule-selector (selector &rest rest &key &allow-other-keys)
    ""
    (declare (ignore rest))
    (cond ((null selector)
           selector)
          ((and (listp selector)
                (member (car selector) '(and or) :test #'equalp))
           (cond ((equalp (car selector) 'or)
                  (ensure-list (cdr selector)))
                 ((equalp (car selector) 'and)
                  (let ((selector-names (cl-naive-dom::convert-tags
                                         (cdr selector))))
                    `(lambda (element path)
                       (let ((path-names (nreverse
                                          (mapcar #'cl-naive-dom::tag path))))
                         ;;set-difference ignores order and length differences
                         (when (not (set-difference ,selector-names path-names))
                           element)))))))

          ((listp selector)
           (let ((selector-names (cl-naive-dom::convert-tags selector)))
             `(lambda (element path)
                (let ((path-names (append (nreverse (mapcar #'cl-naive-dom::tag path))
                                          (list (cl-naive-dom:tag element)))))
                  (when (or
                         (equalp path-names ',selector-names)
                         (and (intersection ',selector-names path-names)
                              (equal ',selector-names
                                     (reverse (subseq (reverse path-names) 0
                                                      (length ',selector-names))))))
                    element)))))
          (t selector))))

(defmacro define-rule ((name selector &rest rest &key ruleset &allow-other-keys) &body body)
  "Macro used to define rules that can be applied to a DOM.

The macro creates a cl-naive-dom transform with appropriate selector.

SYNTAX:

If the selector is NULL the all elements are selected.

If the SELECTOR is a SYMBOL then all elements with a TAG name that is
eql to SELECTOR will be returned.

If the SELECTOR is a list and the car is AND then if the tag path
contains all TAGS after the AND the element will be selected.

If the SELECTOR is a list and the car is OR then if the current TAG is
a memeber the TAGS after the OR the element will be selected.

If the SELECTOR is a list of SYMBOLS then it represents a PATH or a
partial PATH and all elements that match the path or partial PATH are
returned.

If the SELECTOR is a function then the function will be called to
affect selection. The function signature should be (ELEMENT PATH) and
the function must return what should be selected. What should be
selected is up to the user, but typically it would be an ELEMENT or
ATTRIBUTE or ATTRIBUTE VALUE.

RULESET is the ruleset to add the rule to. If no RULESET is
supplied the CASCADING-RULESET is used.

If the RULESET is supplied but it does not exist a new empty ruleset
is created and registered to add the rule to."

  (declare (ignorable rest))
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     (let ((trans (eval (cl-naive-dom:define-transform (,name ,(rule-selector selector) ())
                          ,@body))))
       (add-rule trans :ruleset (or (and ,ruleset
                                         (ensure-ruleset ,ruleset
                                                         :create-p t))
                                    :cascading)))))

(defun apply-rule (name dom &key (ruleset :cascading))
  "Applies the rule named by NAME from the RULESET to the DOM."
  (let ((trans (find-rule name :ruleset ruleset)))
    (unless trans
      (error "Rule ~A does not exist." name))
    (cl-naive-dom:perform-transform trans dom)))

(defun apply-rules (dom &key (ruleset :cascading))
  "Rules will be applied in the order that they where created. IE
reverse order of the rules list.  If you need more control over the
order then you need sort the rules of the ruleset before they are
passed to APPLY-RULES."
  (let ((ruleset% (ensure-ruleset ruleset)))
    (unless ruleset%
      (error "Ruleset ~A does not exist." ruleset))
    (cl-naive-dom:apply-transform-chain (reverse (rules ruleset%)) dom)))

