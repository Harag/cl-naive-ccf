(defsystem "cl-naive-ccf"
  :description ""
  :version "2022.9.8"
  :author ""
  :licence "MIT"
  :depends-on (:cl-naive-dom)
  :components ((:file "src/package")
               (:file "src/config" :depends-on ("src/package"))
               (:file "src/rules" :depends-on ("src/config"))
               (:file "src/inheritance" :depends-on ("src/rules"))))

