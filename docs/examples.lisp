(defparameter *conf* (define-config (theme)
                       (:theme
                        (:primary
                         (:color "Pink"))
                        (:secondary
                         (:color "Grey")))))

(cl-naive-dom:with-sexp
  (select-setting :secondary
                  :config *conf*))

(cl-naive-dom:with-sexp
  (select-setting '(:primary :color)
                  :config *conf*))

(cl-naive-dom:with-sexp
  (select-setting '(:primary :secondary)
                  :or t
                  :config *conf*))

(define-rule (some-rule :secondary)
  (:eish))

(cl-naive-dom:with-sexp
  (apply-rule 'some-rule (cl-naive-dom:with-dom
                           (:theme
                            (:primary
                             (:color "Pink"))
                            (:secondary
                             (:color "Grey"))))))

(define-rule (some-rule-1 :color)
  (:color (nth (random 5) '("Blue" "Green" "Red" "Yellow" "Purple" "Orange"))))

(cl-naive-dom:with-sexp
  (cl-naive-dom:perform-transform 'some-rule-1 (cl-naive-dom:with-dom
                                                 (:theme
                                                  (:primary
                                                   (:color "Pink"))
                                                  (:secondary
                                                   (:color "Grey"))))))

;;TODO: From here on it aint working any more.

(define-rule (some-rule-2 (:primary :color))
  (:color (nth (random 5) '("Blue" "Green" "Red" "Yellow" "Purple" "Orange"))))

(cl-naive-dom:with-sexp
  (cl-naive-dom:perform-transform 'some-rule-2 (cl-naive-dom:with-dom
                                                 (:theme
                                                  (:primary
                                                   (:color "Pink"))
                                                  (:secondary
                                                   (:color "Grey"))))))

