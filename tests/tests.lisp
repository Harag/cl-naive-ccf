(in-package :cl-naive-ccf.tests)

(defmacro with-current-package (&body body)
  `(let ((*package* (or (find-package #.(package-name *package*))
                        (error "Cannot find the package named ~S"
                               #.(package-name *package*)))))
     ,@body))

(defparameter *conf* (define-config (theme)
                       (:theme
                        (:primary
                         (:color "Pink"))
                        (:secondary
                         (:color "Grey")))))

(defparameter *alt-conf* (define-config (theme-too)
                           (:theme-too
                            (:primary
                             (:color "Pink"))
                            (:secondary
                             (:color "Grey")))))

(testsuite  :config
  (testcase :define-config
            :expected 'theme
            :actual (with-current-package
                      (cl-naive-dom:tag
                       *conf*)))
  (testcase :get-config
            :expected 'theme
            :actual (with-current-package
                      (cl-naive-dom:tag
                       (config 'theme))))
  (testcase :ensure-config
            :expected 'theme-too
            :actual (with-current-package
                      (cl-naive-dom:tag
                       (ensure-config
                        'theme-too))))
  (testcase :ensure-config
            :expected 'theme-too
            :actual (with-current-package
                      (cl-naive-dom:tag
                       (ensure-config
                        'theme-too))))
  (testcase :with-config
            :expected '(theme theme)
            :actual (with-current-package
                      (with-config ('theme)
                        (list
                         (cl-naive-dom:tag cl-naive-ccf::*config*)
                         (cl-naive-dom:tag (current-config))))))
  (testcase :select-setting
            :expected 'secondary
            :actual (with-current-package
                      (cl-naive-dom:tag
                       (car
                        (select-setting :secondary
                                        :config *conf*)))))
  (testcase :select-setting-path
            :expected 'color
            :actual (with-current-package
                      (cl-naive-dom:tag
                       (car
                        (select-setting '(:primary :color)
                                        :config *conf*)))))
  (testcase :select-setting-member
            :expected '(primary secondary)
            :actual (with-current-package
                      (let ((elements (select-setting '(or :primary :secondary)
                                                      :config *conf*)))

                        (list (cl-naive-dom:tag (car elements))
                              (cl-naive-dom:tag (car (cdr elements))))))))

(define-rule (some-rule :secondary :crap t :eish t)
  (:eish))

(define-rule (some-rule-1 :color)
  (:color "Some Color"))

;;TODO: From here on it aint working any more.

(define-rule (some-rule-2 (:primary :color))
  (:color "Clear"))

(testsuite  :rules
  (testcase :register-ruleset
            :expected 'MY-SET
            :actual (with-current-package
                      (register-ruleset
                       'my-set
                       (list
                        (cl-naive-dom:define-transform (my-rule (:secondary)
                                                        (element new-children))
                          (declare (ignore element new-children))
                          (:eish)))
                       :if-exists-replace-p t)
                      (name (ruleset 'my-set))))
  (testcase :define-rule
            :expected 'SOME-RULE
            :actual (with-current-package
                      (cl-naive-dom:transform-name
                       (define-rule (some-rule :secondary :shit t :eish t)
                         (:eish)))))
  (testcase :define-rule-eval
            :expected 'some-rule
            :actual (with-current-package
                      (progn (define-rule (some-rule :secondary :shit t :eish t)
                               (:eish))
                             (cl-naive-dom:transform-name
                              (find-rule 'some-rule)))))

  (testcase :apply-rule
            :expected '(:THEME (:PRIMARY (:COLOR "Pink")) (:EISH))
            :actual (with-current-package
                      (cl-naive-dom:with-sexp
                        (apply-rule 'some-rule (cl-naive-dom:with-dom
                                                 (:theme
                                                  (:primary
                                                   (:color "Pink"))
                                                  (:secondary
                                                   (:color "Grey"))))))))
  (testcase :apply-rule-all-path
            :expected '(:THEME (:PRIMARY (:COLOR "Some Color")) (:SECONDARY (:COLOR "Some Color")))
            :actual (with-current-package
                      (cl-naive-dom:with-sexp
                        (apply-rule 'some-rule-1 (cl-naive-dom:with-dom
                                                   (:theme
                                                    (:primary
                                                     (:color "Pink"))
                                                    (:secondary
                                                     (:color "Grey"))))))))
  (testcase :apply-rule-all-path-2
            :expected '(:THEME (:PRIMARY (:COLOR "Clear")) (:SECONDARY (:COLOR "Grey")))
            :actual (with-current-package
                      (cl-naive-dom:with-sexp
                        (apply-rule 'some-rule-2 (cl-naive-dom:with-dom
                                                   (:theme
                                                    (:primary
                                                     (:color "Pink"))
                                                    (:secondary
                                                     (:color "Grey"))))))))
  (testcase :apply-rules
            :expected '(:THEME (:PRIMARY (:COLOR "Clear")) (:EISH))
            :actual (with-current-package
                      (cl-naive-dom:with-sexp
                        (apply-rules (cl-naive-dom:with-dom
                                       (:theme
                                        (:primary
                                         (:color "Pink"))
                                        (:secondary
                                         (:color "Grey")))))))))

(testsuite  :inheritance
  (testcase :set-allowed-attributes
            :expected '(:color :font :class)
            :actual (with-current-package
                      (set-allowed-attributes :div '(:color :font :class))))
  (testcase :allowed-attributes
            :expected '(:color :font :class)
            :actual (with-current-package
                      (allowed-attributes :div)))
  (testcase :allowed-attribute-p
            :expected '(:color :font :class)
            :actual (with-current-package
                      (allowed-attribute-p :div :color)))
  (testcase :apply-inheritance
            :expected '(:HTML (:BODY :COLOR "pink" (:DIV :COLOR "pink" (:SPAN "Hello World!"))))
            :actual (with-current-package
                      (cl-naive-dom:with-sexp
                        (apply-rules (cl-naive-dom:with-dom
                                       (:html
                                        (:body :color "pink"
                                               (:div
                                                (:span "Hello World!")))))
                                     :ruleset :inheritance)))))

;;(report (run))

(cl-naive-dom:with-sexp
  (apply-rules (cl-naive-dom:with-dom
                 (:html
                  (:body :color "pink"
                         (:div
                          (:span "Hello World!")))))
               :ruleset :inheritance))
